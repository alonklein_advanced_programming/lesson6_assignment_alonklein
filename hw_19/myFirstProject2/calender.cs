﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace myFirstProject2
{
    public partial class calender : Form
    {
        private string userName;
        public calender(string name)
        {
            InitializeComponent();
            userName = name;
            update();


        }

        private void update()
        {
          
            
            //using (bd = );
            FileStream f;
            StreamReader bd;
            if (!(File.Exists(userName + "BD.txt")))
            {
                f = File.Create(Directory.GetCurrentDirectory() + "/" + userName + "BD.txt");
                f.Close();
            }
            else
                f = File.OpenRead(userName + "BD.txt");

            if (f == null)
            {
                Console.WriteLine("File Error");
                return;
            }

            bd = new StreamReader(f);
            string line;
            while ((line = bd.ReadLine()) != null)
            {
                string[] parts = line.Split(',');
                string[] currBirthday = parts[1].Split('/');
                string[] date = monthCalendar1.SelectionRange.Start.ToShortDateString().Split('/');
                if(date[0] == currBirthday[0] && date[1] == currBirthday[1])
                {
                    birthday.Text = "Its " + parts[0]+ "'s Birthday";
                    break;
                }
                
               
            }
            if(bd.ReadLine() == null)
                birthday.Text = " No one has Birthday!";


            bd.Close();
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            update();   
        }
    }
}
